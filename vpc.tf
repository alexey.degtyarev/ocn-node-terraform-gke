resource "google_compute_network" "main" {
  name                    = "ocn"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "main" {
  name                     = "ocn"
  network                  = google_compute_network.main.self_link
  ip_cidr_range            = var.ip_cidr_range
  private_ip_google_access = "true"
  dynamic "secondary_ip_range" {
    for_each = var.secondary_ranges
    content {
      range_name    = secondary_ip_range.key
      ip_cidr_range = secondary_ip_range.value
    }
  }
}

resource "google_compute_router" "main" {
  name    = "ocn"
  network = google_compute_network.main.self_link
  bgp {
    asn = 64514
  }
}

resource "google_compute_address" "main" {
  name = "ocn"
}

resource "google_compute_router_nat" "main" {
  name                               = "ocn"
  router                             = google_compute_router.main.name
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = google_compute_address.main.*.self_link
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
  log_config {
    enable = false
    filter = "ALL"
  }
  subnetwork {
    name                    = google_compute_subnetwork.main.self_link
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}
