# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version = "4.48.0"
  hashes = [
    "h1:RSlZSo8GFKBNzDlQ/dMz3CCV/cPje929exxrQW+n/OQ=",
    "h1:k+3asWyC+EEr3gi4Rq8cSP8kr2JGJGcVuQtBXVYorjM=",
    "zh:19c2910e8c555482c28665ad8bed3b6b790ef9b66c565cbb6a5957f8be83048a",
    "zh:25fcb8e54aacdadca12fc9966745f34ae3918fa2a85ee1c1f084ec73c00eb01c",
    "zh:2e54b7902311b1ad69b0eeb2980a83df3df380042be50bf0785ddfc995de0ad8",
    "zh:607ac01d98beaafc06e967294b47d779c23f370240d81c09e434b2b13f56e63f",
    "zh:639fbefa214bbd83a318f38fa7412cebf1e4f90b53f39bf427b865de54ce8013",
    "zh:75b192f9267c71cad573afd2cd0d080c589660766058a9dd8d244bfcb17e96bc",
    "zh:7f355151dd8641bf605f7c287faf56089a9acc9316f91892bc9de8e56d458569",
    "zh:9bfdb7d504c0369352dd019889a7ad35f24751c756eaa13e4ab165d2b5c6a6ea",
    "zh:b37e6b5e1863a30ccd39a7c0f583e21823d373383e474a8845d46693fc3404f8",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:f6640c0d8780d2eb465f3fc4a4bcb11a4a0fc7d6db23feb79ca088fc8d081813",
    "zh:f6c0aed1c955399239010704894a8c583c61a53ef8ac732f9307d04c366f5e7d",
  ]
}

provider "registry.terraform.io/hashicorp/google-beta" {
  version     = "4.48.0"
  constraints = ">= 4.47.0, < 5.0.0"
  hashes = [
    "h1:3xwTCzvvbUCFsoBbCeNxjbeG/TeQbAGToB89HWmxkvc=",
    "h1:8XveifwEYAigZPMqJaMQPb19++7/O+YZRWX6xtojc/8=",
    "zh:29c0ce735927b19f964e157bcb33cd22fb41ca8e19d8c0e6d016e9c2b6011c29",
    "zh:351afa769207d5b6079ca8ed29c403f41091576a7ee4767d93585540feacaf87",
    "zh:4bb6772bd37c9c9827bdaf6a51c2481265c274f8297a7d195af7e2aa0764511e",
    "zh:58e5ee91aee655307b5ad03693eedf2e846b36a5d9d010945c6828b916a9aa09",
    "zh:5f29529296c0008a08b75d52d8d216dd03538d6f600df407cf01d62e001fa87b",
    "zh:62ca291e6c601dba862234bda17327b36753c10aa3664a4e357692d8ad40751f",
    "zh:727cd244138a7c3de6cdfbb7d4155e45932895a23e2ed84309cc112983fc2e9e",
    "zh:95124f3c79bc593816f92cbd64d6dd4a49d982ee624b8b9631cd7d597590a8f6",
    "zh:9d9bbbf7015480af88688c50cf93d4dc27732baadb9ea6434d65e5b2cbca6bf2",
    "zh:b7f23cd1a5a15ab915b6129563ba1bdaadf783f44ef3348fd9a4a763751cbcb0",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:f5ac82527b4f69b8a68fc3fd5699a9f2b1ed739d8ca32e800bac3e3788a6b6cc",
  ]
}

provider "registry.terraform.io/hashicorp/helm" {
  version     = "2.8.0"
  constraints = "2.8.0"
  hashes = [
    "h1:U0w0mUT0SwZCR0poGNSxGaZJKWcOiu4GerpGztYBiMM=",
    "h1:abRryu69lsIGXctqjMVoaKqi74eE12Vzd2FLpds1/PI=",
    "zh:1e42d1a04c07d4006844e477ca32b5f45b04f6525dbbbe00b6be6e6ec5a11c54",
    "zh:2f87187cb48ccfb18d12e2c4332e7e822923b659e7339b954b7db78aff91529f",
    "zh:391fe49b4d2dc07bc717248a3fc6952189cfc49c596c514ad72a29c9a9f9d575",
    "zh:89272048e1e63f3edc3e83dfddd5a9fd4bd2a4ead104e67de1e14319294dedf1",
    "zh:a5a057c3435a854389ce8a1d98a54aaa7cbab68aca7baa436a605897aa70ff7e",
    "zh:b1098e53e1a8a3afcd325ecd0328662156b3d9c3d80948f19ba3a4eb870cee2b",
    "zh:b676f949e8274a2b6c3fa41f5428ea597125579c7b93bb50bb73a5e295a7a447",
    "zh:cdf7e9460f28c2dbfe49a79a5022bd0d474ff18120d340738aa35456ba77ebca",
    "zh:e24b59b4ed1c593facbf8051ec58550917991e2e017f3085dac5fb902d9908cb",
    "zh:e3b5e1f5543cac9d9031a028f1c1be4858fb80fae69f181f21e9465e366ebfa2",
    "zh:e9fddc0bcdb28503078456f0088851d45451600d229975fd9990ee92c7489a10",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.14.0"
  constraints = "~> 2.10, 2.14.0"
  hashes = [
    "h1:4zSUEWLVFn2Sji7mWT64XQGWwBQVDqTGXGfW4ZBB16U=",
    "h1:FFeFf2j2ipbMlrbhmIv8M7bzX3Zq8SQHeFkkQGALh1k=",
    "zh:1363fcd6eb3c63113eaa6947a4e7a9f78a6974ea344e89b662d97a78e2ccb70c",
    "zh:166352455666b7d584705ceeb00f24fb9b884ab84e3a1a6019dc45d6539c9174",
    "zh:4615249ce5311f6fbea9738b25b6e6159e7dcf4693b0a24bc6a5720d1bfd38d0",
    "zh:5205343f8e6cfa89d2f9a312edddcf263755bc294a5216555c390244df826f17",
    "zh:60b7d9b5da2d1a13bc9cdfe5be75da2e3d1034617dff51ef3f0beb72fe801879",
    "zh:61b73d78ef03f0b38ff567b78f2984089eb17724fd8d0f92943b7e522cf31e39",
    "zh:69dfe1278eecc6049736d74c3fa2d1f384035621ec5d72f8b180e3b25b45b592",
    "zh:7746656be1b437e43f7324898cd4548d7e8cad5308042ba38cb45c4fecbf38fe",
    "zh:7e573462091aaf2e6a37edeee33ee4d8f4c37f9a35c331e0f3a60caf078c88c1",
    "zh:a05e1f02b2385679087a7059944cac7fb1d71dd042601ee4d0d26e9808d14dd5",
    "zh:d8d5d52af1aa55160fec601a1006552d9b6fe21e97313850a1e79bc026e99cfe",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.4.3"
  hashes = [
    "h1:tL3katm68lX+4lAncjQA9AXL4GR/VM+RPwqYf4D2X8Q=",
    "h1:xZGZf18JjMS06pFa4NErzANI98qi59SEcBsOcS2P2yQ=",
    "zh:41c53ba47085d8261590990f8633c8906696fa0a3c4b384ff6a7ecbf84339752",
    "zh:59d98081c4475f2ad77d881c4412c5129c56214892f490adf11c7e7a5a47de9b",
    "zh:686ad1ee40b812b9e016317e7f34c0d63ef837e084dea4a1f578f64a6314ad53",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:84103eae7251384c0d995f5a257c72b0096605048f757b749b7b62107a5dccb3",
    "zh:8ee974b110adb78c7cd18aae82b2729e5124d8f115d484215fd5199451053de5",
    "zh:9dd4561e3c847e45de603f17fa0c01ae14cae8c4b7b4e6423c9ef3904b308dda",
    "zh:bb07bb3c2c0296beba0beec629ebc6474c70732387477a65966483b5efabdbc6",
    "zh:e891339e96c9e5a888727b45b2e1bb3fcbdfe0fd7c5b4396e4695459b38c8cb1",
    "zh:ea4739860c24dfeaac6c100b2a2e357106a89d18751f7693f3c31ecf6a996f8d",
    "zh:f0c76ac303fd0ab59146c39bc121c5d7d86f878e9a69294e29444d4c653786f8",
    "zh:f143a9a5af42b38fed328a161279906759ff39ac428ebcfe55606e05e1518b93",
  ]
}
