import { IBridgeConfigurationOptions, DefaultRegistry } from "@shareandcharge/ocn-bridge"
import { OcpiBridge } from "../ocpi-bridge"
import { ModuleImplementation } from "@shareandcharge/ocn-bridge/dist/models/bridgeConfigurationOptions"
import { OcnDB } from "../models/database/ocn-db"
import { IOcpiBackendConfig } from "../models/ocpi";
import { BackendDB } from "../models/database/backend-db";
import { EventEmitter } from "events";

const events = new EventEmitter()

export const backendConfig: IOcpiBackendConfig = {
    registration: {
        versionsURL: process.env.BACKEND_VERSIONS_URL || "https://api.example.com/ocpi/cpo/versions",
        tokenA: process.env.BACKEND_TOKEN_A || "bc65db48-285a-47db-8652-932915579b35"
    },
    publicURL: (process.env.BACKEND_PUBLIC_URL || "https://api.example.com/ocpi-bridge/backend/").replace(/\/+$/, ""),
    party_id: process.env.BACKEND_PARTY_ID || "INO",
    country_code: process.env.BACKEND_COUNTRY_CODE || "NL",
    business_details: {
        name: process.env.BACKEND_BUSINESS_NAME || "OCPI v2.1.1 bridge to OCN"
    },
    pluggableDB: new BackendDB(),
    events
}

export const ocnBridgeConfig: IBridgeConfigurationOptions = {
    publicBridgeURL: (process.env.BRIDGE_PUBLIC_URL || "https://api.example.com/ocpi-bridge/bridge/").replace(/\/+$/, ""),
    port: Number(process.env.BRIDGE_PORT) || 3000,
    ocnNodeURL: process.env.BRIDGE_OCN_NODE_URL || "https://ocn.everon.dev/",
    roles: [
        {
            country_code: process.env.BRIDGE_COUNTRY_CODE || "NL",
            party_id: process.env.BRIDGE_PARTY_ID || "INO",
            role: process.env.BRIDGE_ROLE || "CPO",
            business_details: {
                name: process.env.BRIDGE_BUSINESS_NAME || "OCN bridge to EMSP"
            }
        }
    ],
    modules: {
        implementation: ModuleImplementation.CUSTOM,
        receiver: ["commands"],
        sender: ["locations", "tariffs", "sessions", "cdrs"]
    },
    pluggableAPI: new OcpiBridge(backendConfig.pluggableDB, "NL", "INO", backendConfig.publicURL, events),
    pluggableDB: new OcnDB(),
    pluggableRegistry: new DefaultRegistry(process.env.BRIDGE_REGISTRY || "local"),
    logger: true,
    dryRun: false
}
