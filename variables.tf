variable "project_id" {
  description = "The project ID to host the cluster in"
  type        = string
}

variable "primary_region" {
  description = "The primary region to be used"
  type        = string
}

variable "primary_zones" {
  description = "The primary zones to be used"
  type        = list(string)
}

variable "ip_cidr_range" {
  description = "The range of internal addresses owned by the subnetwork"
  type        = string
}

variable "secondary_ranges" {
  description = "An array of configurations for secondary IP ranges"
  type        = map(string)
}

variable "api_hostname" {
  description = "The hostname to use"
  type        = string
}
