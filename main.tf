/*
* # OCN Node on GKE with Terraform
* 
* > This work is done as part of the participation in the Lighting Works event
* 
* A solution to run an Open Charging Network (OCN) Node in a GKE cluster
* deployed using Terraform.
* 
* ## Open Charging Network
* 
* The Open Charging Network allows you to have simple, cost-efficient and secure
* technical connections to other EV charging players like Charge Point Operators
* and eMobility Service Providers.
* 
* OCN Node implements the Open Charge Point Interface (OCPI) v2.2 API using
* blockchain and smart contracts.  The difference to traditional OCPI hubs is
* that there is not only a centralized server that everybody needs to connect to,
* but decentralized run nodes, OCN Nodes.
* 
* ## Cloud Configuration
* 
* This solution brings you the following:
* - a private GKE cluster (using the official GKE module)
* - a deployment of OCN Node (using the official Docker image)
* - a GCLB HTTPS endpoint to communicate with the OCN node
*   - hosted by Google Cloud DNS
*   - issued by Let's Encrypt
* 
* It can be used as a first step to participate in the decentralised operation of
* the Open Charging Network.
* 
* ## Next steps
*
* - Add Google KMS for secrets handling
* - Add ConfigSync for continuous deployments
* - Add SQL database to persist data across restarts
* - Make the code public
*/

data "google_client_config" "main" {}

provider "google" {
  project = var.project_id
  region  = var.primary_region
}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.main.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}

provider "helm" {
  kubernetes {
    host                   = "https://${module.gke.endpoint}"
    token                  = data.google_client_config.main.access_token
    cluster_ca_certificate = base64decode(module.gke.ca_certificate)
  }
}

module "gke" {
  source                   = "terraform-google-modules/kubernetes-engine/google//modules/beta-private-cluster"
  version                  = "24.1.0"
  project_id               = data.google_client_config.main.project
  name                     = "ocn"
  regional                 = false
  region                   = var.primary_region
  zones                    = var.primary_zones
  release_channel          = "REGULAR"
  enable_private_endpoint  = false
  enable_private_nodes     = true
  gce_pd_csi_driver        = true
  network                  = google_compute_network.main.name
  subnetwork               = google_compute_subnetwork.main.name
  ip_range_pods            = "gke-pods"
  ip_range_services        = "gke-services"
  remove_default_node_pool = true
  master_ipv4_cidr_block   = "192.168.1.0/28"
  master_authorized_networks = [{
    cidr_block   = "0.0.0.0/0"
    display_name = "Public"
  }]
  node_pools = [
    {
      name         = "main"
      autoscaling  = true
      auto_upgrade = true
      preemptible  = true
      node_count   = 3
      machine_type = "e2-standard-2"
    }
  ]
}

resource "google_dns_managed_zone" "main" {
  name     = "ocn"
  dns_name = var.api_hostname
}

resource "google_dns_record_set" "main" {
  type         = "A"
  name         = google_dns_managed_zone.main.dns_name
  managed_zone = google_dns_managed_zone.main.name
  rrdatas      = [kubernetes_ingress_v1.main.status.0.load_balancer.0.ingress.0.ip]
  ttl          = 30
}

resource "kubernetes_ingress_v1" "main" {
  wait_for_load_balancer = true
  metadata {
    name = "ocn"
    annotations = {
      "ingress.gcp.kubernetes.io/pre-shared-cert" = google_compute_managed_ssl_certificate.main.name
      "kubernetes.io/ingress.allow-http"          = false
    }
  }
  spec {
    default_backend {
      service {
        name = "ocn-node"
        port {
          number = 8080
        }
      }
    }
    rule {
      http {
        path {
          path = "/rpc/*"
          backend {
            service {
              name = "ganache"
              port {
                number = 8544
              }
            }
          }
        }
      }
    }
  }
}

resource "google_compute_managed_ssl_certificate" "main" {
  name = "ocn"
  managed {
    domains = [google_dns_managed_zone.main.dns_name]
  }
}

resource "helm_release" "postgresql" {
  name       = "postgresql"
  chart      = "postgresql"
  version    = "12.0.0"
  repository = "https://charts.bitnami.com/bitnami"

  set {
    name  = "auth.postgresPassword"
    value = "postgres"
  }
}
