# OCN Node on GKE with Terraform

> This work was carried out as part of:
> - the Lighting Works event in 2021
> - the Innovation sprint in 2022

A solution to run an Open Charging Network (OCN) Node in a GKE cluster
deployed using Terraform.

## Open Charging Network

The [Open Charging Network][OCN] allows you to have simple, cost-efficient and secure
technical connections to other EV charging players like Charge Point Operators
and eMobility Service Providers.

OCN Node implements the Open Charge Point Interface (OCPI) v2.2 API using
blockchain and smart contracts.  The difference to traditional OCPI hubs is
that there is not only a centralized server that everybody needs to connect to,
but decentralized run nodes, [OCN Nodes][OCN node].

[OCN]: https://energy-web-foundation.gitbook.io/energy-web/technology/application-layer/open-charging-network
[OCN node]: https://github.com/energywebfoundation/ocn-node
[OCPI]: https://evroaming.org/ocpi-background/

## Blockchain

Blockchain technology is essential due to its innate ability to enhance
transparency, accountability, and security. It allows transactions to be
recorded in an immutable public ledger, which promotes trust among parties in
the ecosystem. Additionally, the decentralized nature of blockchain reduces the
risk of system failure and prevents any single entity from gaining control.

While Ethereum serves as the system's backbone in our configuration, it is not
a strict requirement. The application can be implemented on any blockchain
platform that supports smart contracts, such as Ethereum, Solana, etc.


## Cloud Configuration

This solution brings you the following:
- a private GKE cluster (using the official Terraform GKE module)
- a deployment of OCN Node (using the official Docker image)
- a deployment of the Ethereum network
  - Ganache as an Ethereum simulator
  - Blockscout as a blockchain explorer
- a GCLB HTTPS endpoint to communicate with the OCN node
  - hosted by Google Cloud DNS
  - issued by Let's Encrypt

It can be used as a first step to participate in the decentralised operation of
the Open Charging Network.

## Use case

The example test setup includes the following parties:
- a CPO entity `INO/NL` with a station `EVB-X001` located in the Netherlands
- an eMSP entity `INO/DE` with a token `ABCDEF-001` issued in Germany
- an EV-platform powered by the [Everon API][Everon] for the OCPP and OCPI support
- EVB-X001 is powered by the open source [OCPP Simulator][Simulator] by EVBox

The success story of this project is to "charge an EV" using the token at the
station through a roaming connection that is made between the parties
participating in the OCN network.  Charging is simulated by starting and
stopping a session on the station throught the OCN node using the RFID token.
Upon completion, a valid Charge Details Record (CDR) is provided.

Note: Everon API supports the OCPI version 2.1.1, while OCN supports the newer
OCPI version 2.2, therefore a [bridge OCPI v2.1-to-v2.2][Bridge] is required.

Note: Due to the limited support of EMSP interfaces in the OCPI bridge, roaming
of tokens is not yet possible with a OCPI v2.1.1 backend.  Therefore, in the
example use-case we can only use tokens issued by the CPO.

[Everon]: https://docs.everon.io/developers
[Simulator]: https://github.com/evbox/station-simulator
[Bridge]: https://bitbucket.org/shareandcharge/ocpi-2.1.1-bridge

## Next steps

- Add Google KMS for secrets handling
- Add ConfigSync for continuous deployments
- Make the code public

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| google | n/a |
| kubernetes | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| gke | github.com/terraform-google-modules/terraform-google-kubernetes-engine//modules/beta-private-cluster |  |

## Resources

| Name | Type |
|------|------|
| [google_compute_address.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_managed_ssl_certificate.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_managed_ssl_certificate) | resource |
| [google_compute_network.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_network) | resource |
| [google_compute_router.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_router) | resource |
| [google_compute_router_nat.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_router_nat) | resource |
| [google_compute_subnetwork.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork) | resource |
| [google_dns_managed_zone.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dns_managed_zone) | resource |
| [google_dns_record_set.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dns_record_set) | resource |
| [kubernetes_ingress.main](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/ingress) | resource |
| [google_client_config.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config) | data source |

## Inputs

| Name | Description | Required |
|------|-------------|:--------:|
| api_hostname | The hostname to use | yes |
| ip_cidr_range | The range of internal addresses owned by the subnetwork | yes |
| primary_region | The primary region to be used | yes |
| primary_zones | The primary zones to be used | yes |
| project_id | The project ID to host the cluster in | yes |
| secondary_ranges | An array of configurations for secondary IP ranges | yes |

## Outputs

No outputs.
