project_id     = "everon-dev"
primary_region = "europe-west4"
primary_zones  = ["europe-west4-a", "europe-west4-b", "europe-west4-c"]
api_hostname   = "ocn.everon.dev."
ip_cidr_range  = "10.0.0.0/16"
secondary_ranges = {
  gke-pods     = "10.1.0.0/16"
  gke-services = "10.2.0.0/16"
}
